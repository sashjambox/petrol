<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru', // Set the language here

    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
               /* 'enablePrettyUrl' => true,
                'showScriptName' => false,
                'rules' => [
                    '' => 'site/index',
                    '<action>'=>'site/<action>',
                ],
               */ // REST routes for CRUD operations
                ''      => 'action/index',
                '<action>'      => 'action/<action>',
                'site/<'      => 'site/index',
                'POST <controller:\w+>s' => '<controller>/create', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
                '<controller:\w+>s'      => '<controller>/index',
                'PUT <controller:\w+>/<id:\d+>'    => '<controller>/update', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
	            'DELETE <controller:\w+>/<id:\d+>' => '<controller>/delete', // 'mode' => UrlRule::PARSING_ONLY will be implicit here
	            '<controller:\w+>/<id:\d+>'        => '<controller>/view',
                // normal routes for CRUD operations
                '<controller:\w+>s/create' => '<controller>/create',
                '<controller:\w+>/<id:\d+>/<action:update|delete>' => '<controller>/<action>',
            ],
        ],

    ],
    'params' => $params,
];
