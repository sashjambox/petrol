<?php
namespace frontend\models;
use yii\base\Model;

class SaleType extends Model
{
    public static function getList(){
        return  [
            '0' => 'сумма от цены',
            '1' => 'процент от цены'
        ];
    }
}