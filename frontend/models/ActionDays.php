<?php
/**
 * Created by PhpStorm.
 * User: sashjam
 * Date: 19.11.2017
 * Time: 22:30
 */

namespace frontend\models;


use yii\base\Model;

class ActionDays extends Model
{
    public static function getList()
    {
        return ['Все', 'Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.', 'Вскр.'];
    }
}