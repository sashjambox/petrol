<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ActionSearch represents the model behind the search form about `app\models\Action`.
 */
class ActionSearch extends Action
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'type', 'shop_id'], 'integer'],
            [['date_start', 'date_end', 'description', 'pos_article', 'share_properties'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Action::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'shop_id' => $this->id,
            //'date_start' => $this->date_start,
            //'date_end' => $this->date_end,
            'type' => $this->type,
            'shop_id' => $this->shop_id,
        ]);
        $query->andFilterWhere(['like', 'date_start', $this->date_start]);
        $query->andFilterWhere(['like', 'date_end', $this->date_end]);
        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'pos_article', $this->pos_article])
            ->andFilterWhere(['like', 'share_properties', $this->share_properties]);

        return $dataProvider;
    }
}
