<?php
namespace frontend\models;
use yii\base\Model;
class Fuel extends Model
{
    public static function getList()
    {
        return [1 => 'АИ92', 2 => 'АИ95', 3 => 'АИ98', 4 => 'ДТ',];
    }
}