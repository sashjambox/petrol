<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "t_shop".
 *
 * @property integer $id_shop
 * @property string $address
 * @property integer $dtype
 * @property string $name
 */
class Shops extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dtype'], 'integer'],
            [['name'], 'required'],
            [['address'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_shop' => Yii::t('frontend\messages\shareproperies', 'Id Shop'),
            'address' => Yii::t('frontend\messages\shareproperies', 'Address'),
            'dtype' => Yii::t('frontend\messages\shareproperies', 'Dtype'),
            'name' => Yii::t('frontend\messages\shareproperies', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return ShopsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopsQuery(get_called_class());
    }

    public static function getList()
    {
        $shops = [];
        foreach (self::find()->select(['id_shop','name'])->asArray()->all() as $shop)
            $shops[$shop['id_shop']] = $shop['name'];
        return $shops;
    }
}
