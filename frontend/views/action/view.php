<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Action */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Акции'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить акцию?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'label' => 'Описание акции',
                'value' => $model->description,
            ],
            [
                'label' => 'Дата начала акции',
                'value' => $model->date_start,
            ],
            [
                'label' => 'Дата начала акции',
                'value' => $model->date_end,
            ],

            'type',
            'pos_article:ntext',
           'share_properties',
           /* 'share_properties_00',
            'share_properties_10',
            'share_properties_20',
            'share_properties_30',
            'share_properties_40',
            'share_properties_41',*/
            'shop_id',
        ],
    ]) ?>

</div>
