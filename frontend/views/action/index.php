<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Акции');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать Акцию'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{pager}\n{summary}\n{items}\n{pager}",
        'filterModel' => $searchModel,
        'pager'=> [
            'firstPageLabel'=>'⇚',
            'prevPageLabel'=>'⇦',
            'nextPageLabel'=>'⇨',
            'lastPageLabel'=>'⇛',
            'maxButtonCount'=>13,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'description',
            'date_start',
            'date_end',
            //'type',
            [
                'attribute'=>'pos_article',
                'label'=>'Топливо',
                'value' =>  function($model){
                    $fuels = \frontend\models\Fuel::getList();
                    //$type = $model->idShop->name;
                    return $fuels[$model->pos_article];
                } ,
                'filter' => \frontend\models\Fuel::getList(),
            ],
            [   'attribute'=>'shop_id',
                'label'=>'АЗС',
                'filter' => \frontend\models\Shops::getList(),
                'value'=>
                    function($model){
                        $type = $model->idShop->name;
                        return $type;
                    } ,
                /*['width'=>'100px']*/
            ],
            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'copy' => function ($url, $model) {
                        $customurl = Yii::$app->getUrlManager()->createUrl(['action/copy', 'id' => $model['id']]);//$model->id для AR
                        return \yii\helpers\Html::a(
                            '<span class="glyphicon glyphicon-copy"></span>',
                            $customurl,
                            ['title' => Yii::t('yii', 'Копировать'), 'data-pjax' => '1']);
                    },
                ],
                'options' => [
                    'data-pjax' => true,
                ],
                'template' => '{view}&nbsp;|&nbsp;{copy}&nbsp;|&nbsp;{update}&nbsp;|&nbsp;{delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
