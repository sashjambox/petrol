<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Action */

$this->title = Yii::t('app', 'Создать Акцию');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Акции'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <? Pjax::begin();?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?Pjax::end();?>
</div>
