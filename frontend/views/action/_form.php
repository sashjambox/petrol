<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\datetime\DateTimePicker;
use yii\bootstrap;
use yii\widgets\Pjax;
use frontend\models\Shops;
use frontend\models\Fuel;
use richardfan\widget\JSRegister;

?>
<?/**
**@var $model frontend\models\Action
**/?>
<? Pjax::begin([]); ?>
<?$share = $model->getShare()?>

            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($model, 'date_start')->widget(DateTimePicker::className(), [
                        'name' => 'datetime_10',
                        'options' => ['placeholder' => 'Дата начала акции'],
                        'convertFormat' => false,
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd HH:ii:ss',
                            'startDate' => date('yyyy-mm-dd H:i:s'),//''01-Mar-2014 12:00 AM'),
                            'todayHighlight' => true,
                            'clearButton' => false,
                        ]
                    ]); ?>
                </div>
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($model, 'date_end')->widget(DateTimePicker::className(), [
                        'name' => 'datetime_10',
                        'options' => [
                            'placeholder' => 'Дата окончания акции'
                        ],
                        'convertFormat' => false,
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd HH:ii:ss',
                            'startDate' => date('d-M-Y H:i:s'),
                            'todayHighlight' => true
                        ]
                    ]); ?>
                </div>
                <div class="col-xs-12 col-md-12">
                    <?= $form->field($model, 'description')
                        ->textarea(['maxlength' => true])
                        ->label('Описание акции') ?>
                </div>
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($model, 'type')
                        ->textInput(['min' => 1,
                            'max' => 100,
                            'step' => 1,
                            'type' => 'number',
                            'value' => 1,
                            'placeholder' => ''])
                        ->label('Тип')
                    ?>
                </div>
                <div class="col-xs-12 col-md-6">
                    <?= $form->field($model, 'pos_article')
                        ->dropDownList(Fuel::getList())
                        ->label('Тип топлива')
                    ?>
                </div>

                <div class="col-xs-12 col-md-12">
                    <?= $form->field($model, 'shop_id')
                        ->dropDownList(Shops::getList())
                        ->label('АЗС на которой действует акция')
                    ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-12 ">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group field-action-type required">
                                <?= Html::label('Акция начинает действовать если залито литров', 'action-obem',
                                    ['class' => 'control-label', 'for' => 'action-obem']) ?>
                                <?= Html::textInput('Action[share_properties][0]', $share[0] ? $share[0]:1, [
                                    'min' => 0,
                                    'max' => 100,
                                    'step' => 1,
                                    'type' => 'number',
                                    'value' => 1,
                                    'placeholder' => '',
                                    'label' => 'test',
                                    'class' => 'form-control',
                                    'id' => 'action-obem'
                                ]) ?>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group field-action-type required">
                                <?= Html::label('Величина скидки', 'action-saleval') ?>
                                <?= Html::textInput('Action[share_properties][1]', $share[1] ? $share[1]:0, [
                                    'min' => 0,
                                    'max' => 100,
                                    'step' => 0.1,
                                    'type' => 'number',
                                    'placeholder' => 'Величина скидки от 0 до 100 (шаг 0.1)',
                                    'class' => 'form-control',
                                    'id' => 'action-saleval'
                                ])
                                ?>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group field-action-type required">
                                <?= Html::label('Скидка рассчитывается в виде', 'action-saleview') ?>
                                <?= Html::dropdownList('Action[share_properties][2]', $share[2] ? $share[2]:0,
                                    \frontend\models\SaleType::getList(),
                                    ['class' => 'form-control',
                                        'id' => 'action-saleview'
                                    ])
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group field-action-type required">
                                <?= Html::label('Дни недели действия акции', 'action-saledays') ?>
                                <?= Html:: checkboxList('Action[share_properties][3]', $share[3] ? $share[3]:'',
                                    \frontend\models\ActionDays::getList(),
                                    ['class' => ["check_all"], 'separator' => '&nbsp;'])
                                // ->hint('Не выбран день')
                                // ->label('Дни недели действия акции')  ?>
                            </div>
                        </div>
                        <?php JSRegister::begin(); ?>
                        $('form .check_all ').on('change', 'input', function()
                        {
                            var prop = $(this).prop('checked');
                            var val = $(this).val();
                            if (val == 0)
                            {
                                $('form .check_all input').prop('checked',false);
                                $(this).prop('checked',prop);
                            }
                            else if (prop)
                            {
                                $('form .check_all input[value=0]').prop('checked',false);
                            }
                            if ( $('form .check_all input:checked').length == 7 && $('form .check_all input[value=0]').prop('checked')==false)
                            {
                                $('form .check_all input').prop('checked',false);
                                $('form .check_all input[value=0]').prop('checked',true);
                            }
                        });
                        <?php JSRegister::end(); ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group field-action-type required">
                                <?= Html::label('Время действия акции  c', 'action-salefrom') ?>
                                <?= TimePicker::widget([
                                    'name' => 'Action[share_properties][4][0]',
                                    'value' => $share[4][0] ? $share[4][0] : '00:00',
                                    'pluginOptions' => [

                                        'showMeridian' => false,
                                        'showSeconds' => false,
                                        'minuteStep' => 1
                                    ]
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group field-action-type required">
                                <?= Html::label('до  ', 'action-saletill') ?>
                            <?= TimePicker::widget([
                                    'name' => 'Action[share_properties][4][1]',
                                'value' => $share[4][1] ? $share[4][1] : '00:00',
                                'pluginOptions' => [

                                    'showMeridian' => false,
                                    'showSeconds' => false,
                                    'minuteStep' => 1
                                ]
                            ]);?>
                            </div>
                        </div>
                        <?= Html::textInput('Action[share_properties][5]', $share[5]?$share[5]:0,[
                                'hidden'=>true
                        ])
                        ?>
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord
                                    ? Yii::t('app', 'Создать')
                                    : Yii::t('app', 'Обновить'),
                                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

<? Pjax::end([]); ?>