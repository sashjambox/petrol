-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `t_action` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` int(4) NOT NULL,
  `shop_id` int(12) unsigned NOT NULL,
  `share_properties` varchar(1024) DEFAULT NULL,
  `pos_article` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_action` (`id`, `date_start`, `date_end`, `description`, `type`, `shop_id`, `share_properties`, `pos_article`) VALUES
(28,	'2017-09-25 00:00:30',	'2017-12-01 22:35:30',	'Поздняя осень',	1,	2,	'7$0.6$1$1,2,3,4$12:00-14:00$',	'2'),
(29,	'2017-12-01 00:00:31',	'2018-01-01 22:40:31',	'Cristmass Time',	1,	2,	'2$0.1$1$0$00:00-23:59$',	'3'),
(30,	'2017-04-30 00:00:00',	'2017-06-29 00:00:00',	'Дизель 10+',	1,	2,	'10$1.5$0$0$00:01-23:59$0',	'4')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `date_start` = VALUES(`date_start`), `date_end` = VALUES(`date_end`), `description` = VALUES(`description`), `type` = VALUES(`type`), `shop_id` = VALUES(`shop_id`), `share_properties` = VALUES(`share_properties`), `pos_article` = VALUES(`pos_article`);

CREATE TABLE `t_action_type` (
  `type` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `t_shop` (
  `id_shop` smallint(6) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `dtype` smallint(6) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_shop` (`id_shop`, `address`, `dtype`, `name`) VALUES
(1,	'',	0,	'Магазин Чистенькое АЗС  60'),
(2,	' ',	0,	'Магазин Москольцо АЗС  6')
ON DUPLICATE KEY UPDATE `id_shop` = VALUES(`id_shop`), `address` = VALUES(`address`), `dtype` = VALUES(`dtype`), `name` = VALUES(`name`);

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(2,	'admin',	'pGRNpRWJnrkmHIrfII8zhDAFr6AHYwcU',	'$2y$13$NczRMjJostFqES.cinnLzurWJp6cCIrt/4az./ieT5TSYvdE1uBl2',	NULL,	'sashjambox@gmail.com',	10,	1509882693,	1509882693)
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `username` = VALUES(`username`), `auth_key` = VALUES(`auth_key`), `password_hash` = VALUES(`password_hash`), `password_reset_token` = VALUES(`password_reset_token`), `email` = VALUES(`email`), `status` = VALUES(`status`), `created_at` = VALUES(`created_at`), `updated_at` = VALUES(`updated_at`);

-- 2017-11-19 21:09:50
